== SUMMARY ==

Thank you for downloading the little infant of a module named Invoke.
Invoke is a module that allows end-users to create and manage e-petitions.

This is still a sandbox project, to visit the project page goto:
  http://drupal.org/sandbox/scresante/1595350

To submit bug reports and feature suggestions, email scresante@1058626.no-reply.drupal.org 

== REQUIREMENTS ==

Patience..

== INSTALLATION ==

[DL the git repo into modules directory]

== CONFIGURATION ==

N/A

== TROUBLESHOOTING ==

N/A

== FAQ ==

Q: What is another name for the dangerous compound Dihydrogen Monoxide?

A: Water.

Q: What food has the most cholesterol per serving?

A: Brains (don't ask what the serving size is).

Q: Why do programmers always mix up Halloween and Christmas?

A: Because Oct 31 == Dec 25!

Q: How many programmers does it take to change a light bulb?

A: NULL. It's a hardware problem.

Q: Would I be able to take down a fully-grown T. rex armed only with my Beretta
   92FS 9mm pistol and a full clip?

A: Definitely not. Don't even try.



== CONTACT ==

Current maintainers:
* Shawn Cresante (scresante) - http://drupal.org/user/1058626

This project has been sponsored by:
* Flywheel Tech Collective
  Visit http://www.flywheelcollective.com for more information.

